"""
	(Pseudo) OpenStreetMap reader for GDScript
		by あるる / きのもと 結衣 @arlez80

	！！！！！必要な部分しか読みとりません！！！！！
"""

class OSMGeometry:
	var latitude:float = 0.0
	var longitude:float = 0.0

	func _init( lat:float = 0.0, lon:float = 0.0 ):
		self.latitude = lat
		self.longitude = lon

class OSMNode:
	var id:int
	var visible:bool
	var geom:OSMGeometry
	# var user:String

	func _init( parser:XMLParser ):
		self.id = int( parser.get_named_attribute_value( "id" ) )
		self.visible = bool( parser.get_named_attribute_value( "visible" ) )
		self.geom = OSMGeometry.new(
			float( parser.get_named_attribute_value( "lat" ) ),
			float( parser.get_named_attribute_value( "lon" ) )
		)
		# self.user = parser.get_named_attribute_value( "user" )

class OSMWay:
	var id:int
	var uid:int
	var visible:bool = false
	var nodes:Array = []
	var name:String = ""
	var name_en:String = ""
	var type:String = ""
	var sub_type:String = ""
	var height:int = 15.0
	var amenity:String = ""
	var width:float = 0
	var lanes:int = 0

	var center:OSMGeometry

	func _init( parser:XMLParser, all_nodes:Dictionary ):
		self.id = int( parser.get_named_attribute_value( "id" ) )
		self.uid = int( parser.get_named_attribute_value( "uid" ) )
		self.visible = parser.get_named_attribute_value( "visible" ) == "true"

		var sum_lat:float = 0.0
		var sum_lon:float = 0.0

		while parser.read( ) == 0:
			var type:int = parser.get_node_type( )
			if type == XMLParser.NODE_ELEMENT:
				match parser.get_node_name( ):
					"nd":
						var node:OSMNode = all_nodes.get( int( parser.get_named_attribute_value( "ref" ) ) )
						if node != null:
							self.nodes.append( node )
							sum_lat += node.geom.latitude
							sum_lon += node.geom.longitude
					"tag":
						var k:String = parser.get_named_attribute_value("k")
						var v:String = parser.get_named_attribute_value("v")
						match k:
							"name":
								self.name = v
							"name:en":
								self.name_en = v
							"amenity":
								self.amenity = v
							"lanes":
								self.lanes = int( v )
							"width":
								self.width = float( v )
							"building", "natural", "crossing", "fountain", "highway":
								self.type = k
								self.sub_type = v
							"building:levels":
								self.height = int( v )
							_:
								pass
			elif type == XMLParser.NODE_ELEMENT_END:
				break

		self.center = OSMGeometry.new(
			sum_lat / self.nodes.size( ),
			sum_lon / self.nodes.size( )
		)

class OpenStreetMap:
	var bounds_min:OSMGeometry
	var bounds_max:OSMGeometry

	var ways:Array = []

	func _init( ):
		self.bounds_min = OSMGeometry.new( )
		self.bounds_max = OSMGeometry.new( )

	func read( file_path:String ):
		var parser: = XMLParser.new( )
		parser.open( file_path )
		self._read_nodes( parser )

	func read_buffer( src:PoolByteArray ):
		var parser: = XMLParser.new( )
		parser.open_buffer( src )
		self._read_nodes( parser )

	func _read_nodes( parser:XMLParser ):
		var nodes:Dictionary = {}

		while parser.read( ) == 0:
			var type:int = parser.get_node_type( )
			if type == XMLParser.NODE_ELEMENT:
				match parser.get_node_name( ):
					"node":
						var node:OSMNode = OSMNode.new( parser )
						nodes[node.id] = node
					"way":
						ways.append( OSMWay.new( parser, nodes ) )
					_:
						pass

		for node in nodes:
			nodes.erase( node )
