"""
	国土交通省の国土地理院のAPIから標高地図を取得する
		by あるる / きのもと 結衣 @arlez80
	Get elevation map from API Geospatial Information Authority (Ministry of Land, Infrastructure, Transport and Tourism) of Japan
		by Yui Kinomoto @arlez80

	@see http://maps.gsi.go.jp/development/elevation.html
	@see https://maps.gsi.go.jp/development/ichiran.html
"""

extends Node

var dem_url_list:Array = [
	{
		"title": "DEM5A",
		"url": "https://cyberjapandata.gsi.go.jp/xyz/dem5a_png/{z}/{x}/{y}.png",
		"minzoom": 1,
		"maxzoom": 15,
		"fixed": 1
	},
	{
		"title": "DEM5B又は5C",
		"url": "https://cyberjapandata.gsi.go.jp/xyz/dem5b_png/{z}/{x}/{y}.png",
		"minzoom": 1,
		"maxzoom": 15,
		"fixed": 1
	},
	{
		"title": "DEM10B",
		"url": "https://cyberjapandata.gsi.go.jp/xyz/dem_png/{z}/{x}/{y}.png",
		"minzoom": 1,
		"maxzoom": 14,
		"fixed": 0
	}
]

signal complete

"""
	指定した緯度経度の標高をmで取得
"""
func get_height_by_lat_and_long( image:Image, lat:float, lon:float, z:int = 15 ) -> float:
	var pixel_coord:Vector2 = self.get_tile_coord( lat, lon, z )[1]
	return self.get_height_by_pixel( image, int( fmod( pixel_coord.x, 256.0 ) ), int( fmod( pixel_coord.y, 256.0 ) ) )

"""
	指定したピクセルの標高をmで取得
"""
func get_height_by_pixel( image:Image, x:int, y:int ) -> float:
	image.lock( )
	var p:Color = image.get_pixel( x, y )
	image.unlock( )

	if p.r8 == 128 and p.g8 == 0 and p.b8 == 0:
		return 0.0

	var d:float = p.r8 * pow( 2, 16 ) + p.g8 * pow( 2, 8 ) + p.b8
	var h:float = d if d < pow( 2, 23 ) else d - pow( 2, 24 ) 
	if -pow( 2, 23 ) == h:
		h = 0.0
	else:
		h *= 0.01

	return h

"""
	MLITからダウンロードする
"""
func http_get_height_image( lat:float, lon:float, z:int = 15 ) -> Image:
	var downloaded:Image = null
	var main_loop:MainLoop = Engine.get_main_loop( )
	var tree:Viewport = main_loop.get_root( )

	for dem_url in self.dem_url_list:
		if z < dem_url.minzoom or dem_url.maxzoom < z:
			continue

		var tile:Vector2 = self.get_tile_coord( lat, lon, z )[0]
		var url:String = dem_url.url.format({"x": int(tile.x), "y": int(tile.y), "z": z})

		var http_request:HTTPRequest = HTTPRequest.new( )
		yield( main_loop, "physics_frame" )
		tree.add_child( http_request )

		print( "make request %s" % url )
		if http_request.request( url ) != OK:
			# push_error( "An error occured in the HTTP request!" )
			tree.remove_child( http_request )
			http_request.queue_free( )
			continue
		print( "... done" )

		print( "wait for request_completed" )
		var results:Array = yield( http_request, "request_completed" )
		tree.remove_child( http_request )
		http_request.queue_free( )
		print( "... done" )

		# results = [ result, response_code, headers, body ]
		print( results )
		if results[1] == 404:
			continue

		downloaded = Image.new( )
		downloaded.load_png_from_buffer( results[3] )
		break

	self.emit_signal( "complete" )

	return downloaded

"""
	タイル座標を取得
	@return [ tile number, pixel coord in the tile ]
"""
func get_tile_coord( lat:float, lon:float, z:int ) -> Array:
	var r:float = 128.0 / PI

	var lon_rad:float = deg2rad( lon )
	var world_coord_x:float = r * ( lon_rad + PI )
	var pixel_coord_x:float = world_coord_x * pow( 2, z )
	var tile_coord_x:float = floor( pixel_coord_x / 256.0 )

	var lat_rad:float = deg2rad( lat )
	var world_coord_y:float = - r / 2.0 * log( ( 1.0 + sin( lat_rad ) ) / ( 1.0 - sin( lat_rad ) ) ) + 128.0
	var pixel_coord_y:float = world_coord_y * pow( 2, z )
	var tile_coord_y:float = floor( pixel_coord_y / 256.0 )

	return [ Vector2( tile_coord_x, tile_coord_y ), Vector2( pixel_coord_x, pixel_coord_y ) ]

"""
	原点aからbへの距離を計算
"""
func get_world_position_by_lat_lon( a_lat:float, a_lon:float, b_lat:float, b_lon:float ) -> Vector3:
	var rad_lat1:float = deg2rad( a_lat )
	var rad_lon1:float = deg2rad( a_lon )
	var rad_lat2:float = deg2rad( b_lat )
	var rad_lon2:float = deg2rad( b_lon )

	var rad_lat_diff:float = rad_lat1 - rad_lat2
	var rad_lon_diff:float = rad_lon1 - rad_lon2

	var rad_lat_average:float = ( rad_lat1 + rad_lat2 ) / 2.0

	var a:float = 6377397.155
	var b:float = 6356078.963
	var e2:float = (a*a - b*b) / (a*a)
	var a1e2:float = a * ( 1.0 - e2 )

	var sin_lat:float = sin( rad_lat_average )
	var w2:float = 1.0 - e2 * (sin_lat * sin_lat)
	var M:float = a1e2 / ( sqrt( w2 ) * w2 )
	var N:float = a / sqrt( w2 )

	return Vector3( N * cos( rad_lat_average ) * rad_lon_diff, 0.0, M * rad_lat_diff )
