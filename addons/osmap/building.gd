"""
	建物を自動生成
		by あるる / きのもと 結衣 (@arlez80)
"""
extends Spatial

const OSM: = preload("osm.gd")

export(Material) var wall_material:Material = null
export(Material) var roof_material:Material = null
export(int, LAYERS_3D_PHYSICS) var collision_mask:int = 2
export(int, LAYERS_3D_PHYSICS) var collision_layer:int = 16777215
export(float) var level_height:float = 6.0

var center_lat:float
var center_lon:float

onready var wall:MeshInstance = $Wall
onready var roof:MeshInstance = $Roof

var source:OSM.OSMWay = null
var tile_size:float = 0.0
var z:int = 15
var center:Vector3 = Vector3( 0, 0, 0 )

func _ready( ):
	self._generate_mesh( )

func _get_pos2xy( lat:float, lon:float ) -> Vector3:
	var mlit_jp: = preload( "mlit_jp.gd" ).new( )
	var v:Array = mlit_jp.get_tile_coord( lat, lon, self.z )
	return Vector3( v[1].x * tile_size, 0.0, v[1].y * tile_size ) - self.center

"""
	メッシュ生成
"""
func _generate_mesh( ):
	if self.source == null:
		return

	self.center = Vector3( 0, 0, 0 )
	self.center = self._get_pos2xy( self.source.center.latitude, self.source.center.longitude )

	self.level_height = self.source.height

	var path:Array = []
	var sum:Vector3 = Vector3( )

	for node in self.source.nodes:
		var lat:float = node.geom.latitude
		var lon:float = node.geom.longitude
		var v:Vector3 = self._get_pos2xy( lat, lon )
		path.append( v )
		sum += v
		# print( "LATLON( %f, %f ) -> VECTOR3( %f, %f )" % [ lat, lon, v.x, v.z ] )

	var average:Vector3 = sum / path.size( )

	for i in range( path.size( ) ):
		path[i] -= average

	self._update_wall_mesh( path )
	self._update_roof_mesh( path )

"""
	壁を生成
"""
func _update_wall_mesh( path:Array ):
	var faces:Array = []
	var st:SurfaceTool = SurfaceTool.new( )
	st.begin( Mesh.PRIMITIVE_TRIANGLE_STRIP )

	for i in range( path.size( ) ):
		var uvx:float = float(i) / path.size( )
		var vn:Vector3 = path[i].normalized( )

		st.add_uv( Vector2( uvx, 1.0 ) )
		st.add_normal( vn )
		st.add_vertex( path[i] + Vector3( 0, self.level_height, 0 ) )

		st.add_uv( Vector2( uvx, 0.0 ) )
		st.add_normal( vn )
		st.add_vertex( path[i] + Vector3( 0, -5.0, 0 ) )

		var next_i:int = (i+1) % path.size( )
		faces.append( path[i] + Vector3( 0, self.level_height, 0 ) )
		faces.append( path[i] + Vector3( 0, -5.0, 0 ) )
		faces.append( path[next_i] + Vector3( 0, -5.0, 0 ) )
		faces.append( path[i] + Vector3( 0, self.level_height, 0 ) )
		faces.append( path[next_i] + Vector3( 0, -5.0, 0 ) )
		faces.append( path[next_i] + Vector3( 0, self.level_height, 0 ) )

	self.wall.mesh = st.commit( )
	self.wall.cast_shadow = true
	if self.wall_material != null:
		self.wall.material_override = self.wall_material
	else:
		self.wall.material_override = SpatialMaterial.new( )
		self.wall.material_override.params_cull_mode = SpatialMaterial.CULL_DISABLED

	$Wall/StaticBody.collision_mask = self.collision_mask
	$Wall/StaticBody.collision_layer = self.collision_layer

	var wall_shape:ConcavePolygonShape = ConcavePolygonShape.new( )
	wall_shape.set_faces( faces )
	$Wall/StaticBody/CollisionShape.shape = wall_shape

"""
	天井を生成
"""
func _update_roof_mesh( path:Array ):
	var max_x:float = -INF
	var min_x:float = INF
	var max_z:float = -INF
	var min_z:float = INF

	for p in path:
		max_x = max( p.x, max_x )
		min_x = min( p.x, min_x )
		max_z = max( p.z, max_z )
		min_z = min( p.z, min_z )

	if ( max_x - min_x ) == 0 or ( max_z - min_z ) == 0.0:
		return

	var st:SurfaceTool = SurfaceTool.new( )
	st.begin( Mesh.PRIMITIVE_TRIANGLES )

	var polygon:Array = []
	for t in path:
		polygon.append( Vector2( t.x, t.z ) )
	var triangles:PoolIntArray = Geometry.triangulate_polygon( polygon )

	for i in range( int( triangles.size( ) / 3 ) ):
		var a:Vector3 = path[triangles[i*3]]
		var b:Vector3 = path[triangles[i*3+1]]
		var c:Vector3 = path[triangles[i*3+2]]

		st.add_uv( Vector2(
			( a.x - min_x ) / ( max_x - min_x ),
			( a.z - min_z ) / ( max_z - min_z )
		) )
		st.add_normal( Vector3.UP )
		st.add_vertex( a + Vector3( 0, self.level_height, 0 ) )

		st.add_uv( Vector2(
			( b.x - min_x ) / ( max_x - min_x ),
			( b.z - min_z ) / ( max_z - min_z )
		) )
		st.add_normal( Vector3.UP )
		st.add_vertex( b + Vector3( 0, self.level_height, 0 ) )

		st.add_uv( Vector2(
			( c.x - min_x ) / ( max_x - min_x ),
			( c.z - min_z ) / ( max_z - min_z )
		) )
		st.add_normal( Vector3.UP )
		st.add_vertex( c + Vector3( 0, self.level_height, 0 ) )

	self.roof.mesh = st.commit( )
	self.roof.cast_shadow = true
	if self.roof_material != null:
		self.roof.material_override = self.roof_material
	else:
		self.roof.material_override = SpatialMaterial.new( )
		self.roof.material_override.params_cull_mode = SpatialMaterial.CULL_DISABLED

	$Roof/StaticBody.collision_mask = self.collision_mask
	$Roof/StaticBody.collision_layer = self.collision_layer
	$Roof/StaticBody/CollisionShape.shape = self.roof.mesh.create_trimesh_shape( )
