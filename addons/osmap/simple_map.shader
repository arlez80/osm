shader_type spatial;

uniform float uv_scale = 1.0;
uniform sampler2D tex_sand;
uniform sampler2D tex_grass;
uniform sampler2D tex_mud;
uniform sampler2D tex_mountain;
uniform float alt_grass = 1.0;
uniform float alt_mud = 30.0;
uniform float alt_mountain = 50.0;

varying vec3 world_pos;

void vertex( )
{
	world_pos = ( WORLD_MATRIX * vec4( VERTEX, 1.0 ) ).xyz;
}

void fragment( )
{
	vec2 fixed_uv = UV * uv_scale;

	// 砂 -> 草原
	vec3 cur_albedo = mix(
		texture( tex_sand, fixed_uv ).xyz
	,	texture( tex_grass, fixed_uv ).xyz
	,	clamp(
			( world_pos.y / alt_grass )
		,	0.0
		,	1.0
		)
	);

	// 草原 -> 土
	cur_albedo = mix(
		cur_albedo
	,	texture( tex_mud, fixed_uv ).xyz
	,	clamp(
			( world_pos.y / ( alt_mud - alt_grass ) ) - alt_grass
		,	0.0
		,	1.0
		)
	);

	// 土 -> 山の頂上
	cur_albedo = mix(
		cur_albedo
	,	texture( tex_mountain, fixed_uv ).xyz
	,	clamp(
			( world_pos.y / ( alt_mountain - alt_mud ) ) - alt_mud
		,	0.0
		,	1.0
		)
	);

	ALBEDO = cur_albedo;
	ROUGHNESS = 1.0;
	METALLIC = 0.3;
}
