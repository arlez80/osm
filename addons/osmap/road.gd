"""
	道を自動生成
		by あるる / きのもと 結衣 (@arlez80)
"""
extends MeshInstance

const OSM: = preload("osm.gd")

var source:OSM.OSMWay = null
var tile_size:float = 0.0
var z:int = 15
var center:Vector3 = Vector3( 0, 0, 0 )

func _ready( ):
	self._generate_mesh( )

func _get_pos2xy( lat:float, lon:float ) -> Vector3:
	var mlit_jp: = preload( "mlit_jp.gd" ).new( )
	var v:Array = mlit_jp.get_tile_coord( lat, lon, self.z )
	return Vector3( v[1].x * tile_size, 0.0, v[1].y * tile_size ) - self.center

"""
	メッシュ生成
"""
func _generate_mesh( ):
	if self.source == null:
		return

	self.center = Vector3( 0, 0, 0 )
	self.center = self._get_pos2xy( self.source.center.latitude, self.source.center.longitude )

	var path:Array = []
	var sum:Vector3 = Vector3( )

	for node in self.source.nodes:
		var lat:float = node.geom.latitude
		var lon:float = node.geom.longitude
		var v:Vector3 = self._get_pos2xy( lat, lon )
		path.append( v )
		sum += v
		# print( "LATLON( %f, %f ) -> VECTOR3( %f, %f )" % [ lat, lon, v.x, v.z ] )

	var average:Vector3 = sum / path.size( )

	for i in range( path.size( ) ):
		path[i] -= average

	self._update_road_mesh( path )

"""
	道を生成
"""
func _update_road_mesh( path:Array ):
	var st:SurfaceTool = SurfaceTool.new( )
	st.begin( Mesh.PRIMITIVE_TRIANGLE_STRIP )

	for i in range( path.size( ) ):
		var uvx:float = float(i) / path.size( )
		var vn:Vector3 = path[i].normalized( )

		st.add_uv( Vector2( i, 1.0 ) )
		st.add_normal( vn )
		st.add_vertex( path[i] + Vector3( 0, 0.05, 0 ) )

		st.add_uv( Vector2( i, 0.0 ) )
		st.add_normal( vn )
		st.add_vertex( path[i] + Vector3( 0, 0.05, 0 ) )

	self.mesh = st.commit( )
	self.cast_shadow = false
	self.material_override = SpatialMaterial.new( )
	self.material_override.params_cull_mode = SpatialMaterial.CULL_DISABLED
