"""
	OSMap Plugin
		by あるる / きのもと 結衣 (@arlez80)
"""

tool
extends EditorPlugin

func _enter_tree( ):
	self.add_custom_type( "OSMap", "Spatial", preload("map.gd"), preload("icon.png") )

func _exit_tree( ):
	self.remove_custom_type( "OSMap" )

func has_main_screen( ):
	return false

func make_visible( visible:bool ):
	pass

func get_plugin_name( ):
	return "OSMap"
