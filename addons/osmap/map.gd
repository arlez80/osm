"""
	GSIから取得した地図から、メッシュマップを生成
		by あるる / きのもと 結衣 @arlez80
	Generate MeshInstance using get elevation map from MLIT-JP/GSI
		by Yui Kinomoto @arlez80

	@see https://github.com/godotengine/godot/blob/00949f0c5fcc6a4f8382a4a97d5591fd9ec380f8/editor/import/resource_importer_obj.cpp
"""
tool

extends Spatial

class_name OSMap

const OSM: = preload("osm.gd")

export(bool) var enabled_ground:bool = true
export(bool) var enabled_building:bool = true
export(bool) var enabled_ground_collision:bool = true
export(bool) var enabled_building_collision:bool = true

export(int, LAYERS_3D_PHYSICS) var ground_collision_mask:int = 0
export(int, LAYERS_3D_PHYSICS) var ground_collision_layer:int = 0
export(int, LAYERS_3D_PHYSICS) var building_collision_mask:int = 0
export(int, LAYERS_3D_PHYSICS) var building_collision_layer:int = 0

export(float) var latitude:float = 33.163861
export(float) var longitude:float = 129.725731
export(int, 15) var z:int = 15
export(Image) var height_map:Image = null setget set_height_map
export(Material) var ground_material:Material = null setget set_ground_material

var osm_reader:OSM.OpenStreetMap = null setget set_osm_reader

var ground:MeshInstance
var building:Spatial
var ground_static_body:StaticBody
var ground_static_body_collision_shape:CollisionShape

func _init( ):
	for t in self.get_children( ):
		self.remove_child( t )

	self.ground = MeshInstance.new( )
	self.add_child( self.ground )
	self.building = Spatial.new( )
	self.add_child( self.building )

func _ready( ):
	self._generate_ground_surface( )
	self._generate_buildings( )

func set_height_map( _height_map:Image ) -> Image:
	height_map = _height_map
	self._generate_ground_surface( )

	return height_map

func _generate_ground_surface( ):
	if self.ground == null:
		return

	var mlit_jp: = preload( "mlit_jp.gd" ).new( )

	if self.height_map == null:
		# TODO: 自動で読みこむようにする
		#self.height_map = yield( mlit_jp.http_get_height_image( self.latitude, self.longitude, self.z ), "complete" )
		return

	var tile_size:float = self.get_tile_size( )

	var st:SurfaceTool = SurfaceTool.new( )
	st.begin( Mesh.PRIMITIVE_TRIANGLES )
	st.add_smooth_group( true )

	var verts:Array = []
	var uvs:Array = []

	for wz in range( 256 ):
		var v:Array = []
		var vt:Array = []
		var tz:float = wz * tile_size
		for wx in range( 256 ):
			var tx:float = wx * tile_size
			v.append( Vector3( tx, mlit_jp.get_height_by_pixel( self.height_map, wx, wz ), tz ) )
			vt.append( Vector2( wx / 256.0, wz / 256.0 ) )
		verts.append( v )
		uvs.append( vt )

	for wz in range( 255 ):
		for wx in range( 255 ):
			var selected_verts:Array = [
				verts[wz][wx],
				verts[wz][wx+1],
				verts[wz+1][wx],
				verts[wz+1][wx+1],
			]
			var selected_uvs:Array = [
				uvs[wz][wx],
				uvs[wz][wx+1],
				uvs[wz+1][wx],
				uvs[wz+1][wx+1],
			]

			for i in [0,1,2,  2,1,3]:
				st.add_uv( selected_uvs[i] )
				st.add_vertex( selected_verts[i] )

	st.generate_normals( )
	self.ground.mesh = st.commit( )
	self.ground.material_override = self.ground_material
	self.ground.cast_shadow = true

	if self.enabled_ground_collision:
		self.ground_static_body = StaticBody.new( )
		self.ground_static_body.collision_mask = self.ground_collision_mask
		self.ground_static_body.collision_layer = self.ground_collision_layer
		self.ground_static_body_collision_shape = CollisionShape.new( )
		self.ground_static_body_collision_shape.shape = self.ground.mesh.create_trimesh_shape( )
		self.ground_static_body.add_child( self.ground_static_body_collision_shape )
		self.add_child( self.ground_static_body )
	else:
		self.ground_static_body = null

func _generate_buildings( ):
	if self.osm_reader == null:
		return

	var mlit_jp: = preload( "mlit_jp.gd" ).new( )
	var source_building: = preload( "Building.tscn" )

	var zero = mlit_jp.get_tile_coord( self.latitude, self.longitude, self.z )
	var zero_tile_coord_x:int = int( zero[0].x )
	var zero_tile_coord_y:int = int( zero[0].y )

	var tile_size:float = self.get_tile_size( )
	var zero_coord:Vector3 = Vector3( fmod( zero[1].x, 256.0 ) * tile_size, 0.0, fmod( zero[1].y, 256.0 ) * tile_size )
	printt( zero_coord, zero_tile_coord_x, zero_tile_coord_y )

	for way in self.osm_reader.ways:
		if way.type == "building":
			var ttc: = mlit_jp.get_tile_coord( way.center.latitude, way.center.longitude, self.z )
			if int( ttc[0].x ) != zero_tile_coord_x or int( ttc[0].y ) != zero_tile_coord_y:
				# printt( " --> ", ttc[0].x, ttc[0].y )
				continue

			var t: = source_building.instance( )
			t.transform.origin = Vector3(
				fmod( ttc[1].x, 256.0 ) * tile_size,
				mlit_jp.get_height_by_lat_and_long( self.height_map, way.center.latitude, way.center.longitude, self.z ),
				fmod( ttc[1].y, 256.0 ) * tile_size
			)
			t.source = way
			t.z = self.z
			t.tile_size = tile_size
			self.building.add_child( t )

func set_ground_material( _ground_material:Material ) -> Material:
	ground_material = _ground_material
	if self.ground != null:
		self.ground.material_override = ground_material
	return ground_material

func set_osm_reader( _osm_reader:OSM.OpenStreetMap ):
	osm_reader = _osm_reader
	self._generate_buildings( )

func get_tile_size( ) -> float:
	return ( 16 - self.z ) * 5.0
